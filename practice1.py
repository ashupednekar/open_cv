import cv2
import numpy as np 
import matplotlib.pyplot as plt 
 
vid = cv2.VideoCapture('13.Reasons.Why.S01E02.WEB.X264-DEFLATE[eztv].mkv')
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('13output.avi', fourcc, 20.0 , (640,480))

while True:
    ret, frame = vid.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    out.write(frame)
    cv2.imshow('gray',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

vid.release()
out.release()
cv2.destroyAllWindows()