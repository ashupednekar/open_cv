import cv2
import numpy as np 
import matplotlib.pyplot as plt 
 
img = cv2.imread('watch.jpg',cv2.IMREAD_GRAYSCALE) #other options are IMREAD_COLOR ,IMREAD_UNCHANGED ,etc.

# Showing images
#cv2.imshow('image',img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()

#using matplotlib
#plt.imshow(img, cmap='gray', interpolation='bicubic')
#plt.plot([50,100],[50,100],'m',linewidth=5)
#plt.show()

cv2.imwrite('watchgray.png',img)