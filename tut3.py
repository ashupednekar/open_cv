import cv2
import numpy as np 
    
img = cv2.imread('watch.jpg',cv2.IMREAD_COLOR)

#line
cv2.line(img, (0,0), (150,150), (255,255,255), 5)
#rectangle
cv2.rectangle(img, (15,25), (200,150), (155,244,255) ,5)
#circle
cv2.circle(img, (100,63), 55, (155,43,255) , -1)
#polygon
pts = np.array([[10,5],[20,30],[70,20],[50,10]], np.int32)
cv2.polylines(img , [pts], True, (23,33,55), 3)
#text
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img, 'Opencv ROCKS ! Matlab SUCKS!', (0,130), font, 0.5, (100,255,50), 2, cv2.LINE_AA)

cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite('scribling.png',img)